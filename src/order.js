import dishes from './menu.js';

class Order {
  constructor (itemsMap) {
    this.newOrder = dishes
      .filter(dish => itemsMap.has(dish.id))
      .map(dish => {
        return { id: dish.id, name: dish.name, price: dish.price, count: itemsMap.get(dish.id) };
      });
  }

  get itemDetails () {
    return this.newOrder;
  }

  get totalPrice () {
    return this.newOrder
      .map(item => item.price * item.count)
      .reduce((a, b) => a + b, 0);
  }
}

export default Order;
