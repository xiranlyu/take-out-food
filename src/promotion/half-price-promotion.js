import Promotion from './promotion.js';

class HalfPricePromotion extends Promotion {
  constructor (order) {
    super(order);
    this._type = '指定菜品半价';
    this.halfPriceDishes = ['ITEM0001', 'ITEM0022'];
  }

  get type () {
    return this._type;
  }

  includedHalfPriceDishes () {
    return this.order.itemDetails.filter(item => item.id === 'ITEM0001' || item.id === 'ITEM0022');
  }

  discount () {
    return this.order.itemDetails
      .filter(item => item.id === 'ITEM0001' || item.id === 'ITEM0022')
      .map(x => x.price / 2 * x.count)
      .reduce((a, b) => a + b, 0);
  }
}

export default HalfPricePromotion;
