function parseInputToMap(selectedItems) {
  const map = new Map();
  const singleItem = selectedItems.split(',');
  singleItem.map(item => {
    item = item.split(' ');
    map.set(item[0], parseInt(item[item.length - 1]));
  });
  return map;
}

export { parseInputToMap };
